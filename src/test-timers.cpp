﻿#include <iostream>

#include <Windows.h>

#include "easywin/easywin.h"

using namespace easywin;

COLORREF lightColor = Color::green;

void testTimers()
{
	auto window = Window::CreateTopWindow();
	window->setName(u8"Test odmierzania czasu.");

	auto button = Button::CreateButton(window, u8"Pokaż message box za 5s.", RECT{ 100,20,300,50 });
	button->setOnClick([window]() {
		window->callDelayed(5000, [window]() {
			window->showMessageBox(u8"5s minęło", u8"...", MB_OK);
			});
		});

	window->startTimer(1000, [window]() {
		std::cout << "Timer!" << std::endl;
		if (lightColor == Color::green)
			lightColor = Color::red;
		else
			lightColor = Color::green;
		window->invalidate();
		});

	window->setOnDraw([](PaintDC& dc) {

		dc.setPenColor(lightColor);
		dc.setBrushColor(lightColor);
		dc.drawCircle(20, 30, 10);
		return 0;
		});

	window->show();
	window->update();

}