﻿#include <Windows.h>

#include <string>
#include <algorithm>
#include <vector>
#include <memory>

#include "easywin/easywin.h"

using namespace easywin;

class LabirynthGame
{
public:
	int posx = 1;
	int posy = 1;
	int frame = 0;
	const int width = 40;
	const int height = 20;
	const int tile = 32;

	// X - nieznana ściana
	// x - znana ściana
	// . - nieznana podłoga
	//   - (spacja) znana podłoga

	std::string data =
		"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
		"X.XXX.........X........XX....XXXXXXXXXXX"
		"X.X...XXXXXXX.XXX.XXXX.XX.XX..........XX"
		"X.XXX.X.....X.XXXXXXXX.XX.XX.XXXXXXXX.XX"
		"X.....XXX.XXX..........XX.XX.XXXX.XXX.XX"
		"XXXXXXXX....X.X.XXXXX.XXX.XXXX....XXX..X"
		"X........XX.X.XXX.....XXX.XXXXXXX...XX.X"
		"X.XXXX.X.X..X...XXXXX.............XXXX.X"
		"X....X.X.XXXX.X.XXXXXXXXXXXXXXXXX.XXXX.X"
		"X.XX.X.X......X........................X"
		"X..X.X.XXXXXXXX.XXXXXXX.XXXXXXXXX.XX.XXX"
		"XX.X.X.XXX......XXXX.XXXXX.XXX.XX.X...XX"
		"XX.XXX...X.XXXXXXXXX.......XXX.XX.X.X.XX"
		"X..X..XX.X.......X.X.XXXXX.XXX.XX.X.X..X"
		"X.XXX.XX.XXXXXXX.X.XXX.XXX........X.XX.X"
		"X................X.........XXX.XX.X.XX.X"
		"XXXXXXXX.XXXXXXXXX.XXXX.XXXXXX.XX.XXXX.X"
		"X........XX....XXX.XX...XX........X....X"
		"X.XXX.XX....XX.....XX.XXXX..XXXXXXX.XXXX"
		"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";

	char get(int x, int y) { return data[x + y * width]; }
	void set(int x, int y, char val) { data[x + y * width] = val; }

	static const int heroFrames = 4;
	Bitmap::Ptr floor;
	Bitmap::Ptr brick;
	Bitmap::Ptr hero[heroFrames];
	Bitmap::Ptr unknown;

	void init() {
		floor = Bitmap::CreateFromBmpFile("res/floor.bmp");
		brick = Bitmap::CreateFromBmpFile("res/brick.bmp");
		hero[0] = Bitmap::CreateFromBmpFile("res/eyes1.bmp");
		hero[1] = Bitmap::CreateFromBmpFile("res/eyes2.bmp");
		hero[2] = Bitmap::CreateFromBmpFile("res/eyes3.bmp");
		hero[3] = hero[1];
		unknown = Bitmap::CreateFromBmpFile("res/voidwall.bmp");
		moveTo(posx, posy);
	}

	// odkrywa labirynt wokół gracza
	void uncover(int x, int y) {
		for (int ix = x - 1; ix <= x + 1; ix++)
			for (int iy = y - 1; iy <= y + 1; iy++) {
				auto pole = get(ix, iy);
				if (pole == '.') set(ix, iy, ' ');
				if (pole == 'X') set(ix, iy, 'x');
			}
	}

	// sprawdza, czy w podanym polu jest podłoga i jeśli tak, to przesuwa tam gracza
	bool moveTo(int x, int y) {
		auto pole = get(x, y);
		if (pole == '.' || pole == ' ') {
			uncover(x, y);
			posx = x;
			posy = y;
			return true;
		}
		else
			return false;
	}
};

// tu odkładamy wszystkie rozpoczęte gry. Możemy mieć ich kilka na raz w różnych oknach.
std::vector< std::shared_ptr<LabirynthGame> > games;

void testLabirynth()
{
	auto game = std::make_shared<LabirynthGame>();
	games.push_back(game);

	game->init();

	auto window = Window::CreateTopWindow();
	window->setName(u8"Użyj klawiszy strzałek kursora aby poruszać się po labiryncie.");


	// rysujemy mapę oraz postać
	window->setOnDraw([game, window](PaintDC& dc)->LRESULT {
		auto bufor = Bitmap::CreateCompatibile(dc,
			game->width * game->tile,
			game->height * game->tile);
		auto bufordc = bufor->getPaintDC(dc);

		for (int x = 0; x < game->width; x++)
			for (int y = 0; y < game->height; y++) {
				Bitmap::Ptr b;
				auto pole = game->get(x, y);
				switch (pole) {
				case 'x': b = game->brick; break;
				case ' ': b = game->floor; break;
				default:
					b = game->unknown;
				}
				bufordc->drawBitmap(x * game->tile,
					y * game->tile,
					b);
			}
		bufordc->drawBitmap(game->posx * game->tile,
			game->posy * game->tile,
			game->hero[game->frame]);
		dc.drawBitmap(0, 0, bufor);
		return 0;
		});

	// ten timer będzie nam odświerzał okno 10 razy na sekundę przy okazji animując naszą postać (zmieniając klatkę animacji)
	window->startTimer(100, [game, window]() {
		game->frame = (game->frame + 1) % game->heroFrames;
		window->invalidate();
		return 0;
		});


	// obsługa klawiatury
	window->setOnKeyDown([game](WPARAM key) {
		switch (key) {
		case VK_UP:
			game->moveTo(game->posx, game->posy - 1);
			break;
		case VK_DOWN:
			game->moveTo(game->posx, game->posy + 1);
			break;
		case VK_LEFT:
			game->moveTo(game->posx - 1, game->posy);
			break;
		case VK_RIGHT:
			game->moveTo(game->posx + 1, game->posy);
			break;
		}
		return 0;
		});


	// zamknięcie okna musi spowodować usunięcie gry z listy.
	window->setOnClose([window, game]() {
		games.erase(std::remove_if(games.begin(), games.end(), [game](auto g) { return g == game; }));
		window->destroy();
		return 0;
		});

	window->show();
	window->update();

}