﻿#include <iostream>

#include <Windows.h>

#include "easywin/easywin.h"

using namespace easywin;


void testTab()
{
	auto window = Window::CreateTopWindow();
	window->setName(u8"Test Zakładek");

	auto tab = Tab::Create(window, window->getClientRect());

	// tab1
	auto tab1 = tab->addItem(u8"Pierwsza");
	StaticText::Create(tab1, u8"To jest pierwsza zakładka", RECT{ 10,10,300,40 });

	// tab2
	auto tab2 = tab->addItem(u8"Druga");
	StaticText::Create(tab2, u8"To jest druga zakładka", RECT{ 10,10,300,40 });

	// tab3
	auto tab3 = tab->addItem(u8"Trzecia");
	StaticText::Create(tab3, u8"To jest trzecia zakładka", RECT{ 10,10,300,40 });

	window->show();
	window->update();
}