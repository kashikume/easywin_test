﻿#include <Windows.h>
#include <iostream>

#include "easywin/easywin.h"

using namespace easywin;


void testButtons()
{


	auto window = Window::CreateTopWindow();
	window->setName(u8"Buttons Test");

	auto checkbox = Button::CreateCheckbox(window, u8"Zaznaczałka", RECT{ 10, 10, 300, 40 });
	checkbox->setBackground(Color::red);
	auto checkbox_sprawdz = Button::CreateButton(window, u8"Czy zaznaczony?", RECT{ 310, 10, 600, 40 });

	//na początku zaznaczym checkbox:
	checkbox->setCheck(true);

	checkbox->setOnClick([checkbox]() {
		std::cout << "Zmiana stanu checkboxa na " << checkbox->isChecked() << std::endl;
		});

	checkbox_sprawdz->setOnClick([checkbox, window]() {
		bool zaznaczony = checkbox->isChecked();
		window->showMessageBox(zaznaczony ? u8"Zaznaczony!" : u8"Nie zaznaczony", u8"Sprawdzamy:", MB_OK);
		return 0;
		});

	auto radio_1_1 = Button::CreateRadio(window, u8"radio 1/1", Rect{ 10,50,200,70 }, true);
	radio_1_1->setBackground(Color::white);
	auto radio_1_2 = Button::CreateRadio(window, u8"radio 1/2", Rect{ 10,70,200,90 }, false);
	radio_1_2->setBackground(Color::white);
	auto radio_1_3 = Button::CreateRadio(window, u8"radio 1/3", Rect{ 10,90,200,110 }, false);
	radio_1_3->setBackground(Color::white);

	auto radio_2_1 = Button::CreateRadio(window, u8"radio 2/1", Rect{ 10,150,200,170 }, true);
	auto radio_2_2 = Button::CreateRadio(window, u8"radio 2/2", Rect{ 10,170,200,190 }, false);
	auto radio_2_3 = Button::CreateRadio(window, u8"radio 2/3", Rect{ 10,190,200,210 }, false);


	window->show();
	window->update();
}