﻿#include <iostream>

#include <Windows.h>

#include "easywin/easywin.h"

using namespace easywin;

void testWindows()
{
	auto window = Window::CreateTopWindow();

	window->setName(u8"Podstawowe funkcje okien, obserwuj konsolę");

	// zapytamy użytkownika, czy na pewno chce zamknąć okno. W tym celu dodamy własną obsługę przycisku zamykającego [x].
	window->setOnClose([window]() -> LRESULT {
		if (window->showMessageBox(u8"Czy na pewno chcesz zamknąć to okno?", u8"Potwierdzenie", MB_YESNO | MB_ICONQUESTION) == IDYES)
		{
			// jeśli użytkownik kliknął "Yes", niszczymy okno.
			window->destroy();
		}
		return 0;
		});

	window->setOnSize([window](int w, int h, WindowShowStyle mode) {
		std::cout << "Zmiana rozmiaru okna: " << w << " x " << h << std::endl;
		return 0;
		});

	window->setOnDraw([window](PaintDC& dc) {
		std::cout << "Odrysowywanie okna " << window->getHWnd() << std::endl;
		return 0;
		});

	window->show();
	window->update();

}