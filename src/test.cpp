﻿#include <Windows.h>
#include <iostream>
#include "resource.h"

# include "easywin/easywin.h"

using namespace easywin;

void testWindows();
void testModalWindows();
void testBitmaps();
void testTimers();
void testNoBlink();
void testMouse();
void testText();
void testEditBoxes();
void testButtons();
void testLabirynth();
void testLoadSave();
void testTab();
void testScrolls();
void testMenus();

void tests()
{
	auto testMenu = Window::CreateTopWindow();
	testMenu->setName(u8"Tests Menu");
	auto icon = Icon::CreateResIcon(IDI_ICON1);
	testMenu->setIcon(icon);

	//zamknięcie tego okna spowoduje wyjście z programu.
	testMenu->setOnClose([testMenu]()
		{
			testMenu->destroy();
			PostQuitMessage(0);
			return 0;
		});

	testMenu->setFeature(Window::Feature::resizeFrame, false);
	testMenu->setFeature(Window::Feature::maximizeBox, false);

	const int w = 300; // szerokość przycisku
	const int h = 30; // wysokość przycisku
	const int d = 5; // odległość między przyciskami

	int x = d;
	int y = d;

	auto windows = Button::CreateButton(testMenu, u8"Basic windows functions", RECT{ x, y, x + w, y + h });
	windows->setOnClick(testWindows);
	y += h + d;

	auto modals = Button::CreateButton(testMenu, u8"Test modal windows", RECT{ x, y, x + w, y + h });
	modals->setOnClick(testModalWindows);
	y += h + d;

	auto bitmaps = Button::CreateButton(testMenu, u8"Bitmaps", RECT{ x, y, x + w, y + h });
	bitmaps->setOnClick(testBitmaps);
	y += h + d;

	auto timers = Button::CreateButton(testMenu, u8"Timers", RECT{ x, y, x + w, y + h });
	timers->setOnClick(testTimers);
	y += h + d;

	auto noblink = Button::CreateButton(testMenu, u8"No blinking painting", RECT{ x, y, x + w, y + h });
	noblink->setOnClick(testNoBlink);
	y += h + d;

	auto mouse = Button::CreateButton(testMenu, u8"Mouse", RECT{ x, y, x + w, y + h });
	mouse->setOnClick(testMouse);
	y += h + d;

	auto texts = Button::CreateButton(testMenu, u8"Keyboard and text", RECT{ x, y, x + w, y + h });
	texts->setOnClick(testText);
	y += h + d;

	auto editboxes = Button::CreateButton(testMenu, u8"Edit controlls", RECT{ x, y, x + w, y + h });
	editboxes->setOnClick(testEditBoxes);
	y += h + d;

	auto buttons = Button::CreateButton(testMenu, u8"Buttons", RECT{ x, y, x + w, y + h });
	buttons->setOnClick(testButtons);
	y += h + d;

	auto labirynt = Button::CreateButton(testMenu, u8"Labirynth demo", RECT{ x, y, x + w, y + h });
	labirynt->setOnClick(testLabirynth);
	y += h + d;

	auto loadsave = Button::CreateButton(testMenu, u8"Load/Save", RECT{ x, y, x + w, y + h });
	loadsave->setOnClick(testLoadSave);
	y += h + d;

	auto tabs = Button::CreateButton(testMenu, u8"Tab", RECT{ x, y, x + w, y + h });
	tabs->setOnClick(testTab);
	y += h + d;

	auto scrolls = Button::CreateButton(testMenu, u8"Scrolls", RECT{ x, y, x + w, y + h });
	scrolls->setOnClick(testScrolls);
	y += h + d;

	auto menus = Button::CreateButton(testMenu, u8"Menu", RECT{ x, y, x + w, y + h });
	menus->setOnClick(testMenus);
	y += h + d;

	testMenu->setSize(w + 2 * d + 20, y + d + 30);

	testMenu->update();
	testMenu->show();
}

int main()
{
	tests();

	// to zawsze trzeba uruchomić w main na końcu, aby okna działały.
	Window::runMessageLoop();

	return 0;
}
