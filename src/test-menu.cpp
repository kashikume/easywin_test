#include <string>

#include "easywin/easywin.h"

using namespace easywin;

void testMenus()
{
	auto window = Window::CreateTopWindow();
	window->setName(u8"Menu Test");

	auto topMenu = Menu::Create();

	auto fileSubmenu = topMenu->addSubMenu(u8"File");
	fileSubmenu->addMenuItem(u8"Open", [window]() { window->showMessageBox(u8"File/Open", u8""); });
	fileSubmenu->addMenuItem(u8"Save", [window]() { window->showMessageBox(u8"File/Save", u8""); });
	fileSubmenu->addSeparator();
	auto otherSubmenu = fileSubmenu->addSubMenu(u8"Some other submenu");
	otherSubmenu->addMenuItem(u8"Anything", [window]() { window->showMessageBox(u8"File/Other/Anything", u8""); });

	fileSubmenu->addSeparator();
	fileSubmenu->addMenuItem(u8"Exit", [window]() { window->destroy(); });

	auto helpSubmenu = topMenu->addSubMenu(u8"Help");
	helpSubmenu->addMenuItem(u8"About", [window]() { window->showMessageBox(u8"Help/About", u8""); });

	window->setMenu(topMenu);

	window->show();
	window->update();

}