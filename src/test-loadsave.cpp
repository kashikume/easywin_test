﻿#include <Windows.h>

#include <compare>
#include <filesystem>

#include "easywin/easywin.h"

using namespace easywin;

void testLoadSave()
{
	auto window = Window::CreateTopWindow();
	window->setName(u8"Okien wyboru pliku");


	const int w = 300; // szerokość przycisku
	const int h = 30; // wysokość przycisku
	const int d = 5; // odległość między przyciskami

	int x = d;
	int y = d;

	auto loadButton = Button::CreateButton(window, u8"Otwórz Plik", RECT{ x, y, x + w, y + h });
	loadButton->setOnClick([window]() {
		FileDialog fd;
		if (fd.showFileOpenDialog(window)) {
			window->showMessageBox(fd.getResult().u8string(), u8"Wybrano");
		}
		return 0;
		});
	y += h + d;

	auto saveButton = Button::CreateButton(window, u8"Zapisz Plik", RECT{ x, y, x + w, y + h });
	saveButton->setOnClick([window]() {
		FileDialog fd;
		if (fd.showFileSaveDialog(window)) {
			window->showMessageBox(fd.getResult().u8string(), u8"Wybrano");
		}
		return 0;
		});
	y += h + d;

	window->setSize(w + 2 * d + 20, y + d + 30);


	window->show();
	window->update();

}