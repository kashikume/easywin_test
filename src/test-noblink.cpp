﻿#include <string>

#include "easywin/easywin.h"

using namespace easywin;

int odrysowania = 0;

void testNoBlink()
{
	auto window = Window::CreateTopWindow();
	window->setName(u8"Buforowane rysowanie - zapobieganie miganiu");

	// każde kliknięcie myszą w okno spowoduje jego odrysowanie
	window->setOnMouseLeftDown([window](int x, int y, const Modifiers& m) {
		window->invalidate();
		return 0;
		});

	window->setOnBufferedDraw([window](PaintDC::Ptr& dc) {

		Rect size = window->getClientRect();

		dc->setBrushColor(Color::yellow);
		dc->setPenColor(Color::blue);
		// rysujemy na bitmapie
		for (int i = 2; i < 15; ++i) {
			dc->drawCircle(size.width() / 2, size.height() / i, size.height() / i);
		}
		});

	window->show();
	window->update();

}