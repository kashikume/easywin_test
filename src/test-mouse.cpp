﻿#include <iostream>
#include <string>
#include <sstream>

#include <Windows.h>

#include "easywin/easywin.h"

using namespace easywin;

int mouse_x, mouse_y;
bool lewy;
int lewy_x, lewy_y;
bool prawy;
int prawy_x, prawy_y;

void testMouse()
{
	auto window = Window::CreateTopWindow();
	window->setName(u8"Test myszy");

	window->setOnMouseMove([window](int x, int y, const Modifiers& m)->LRESULT {
		mouse_x = x;
		mouse_y = y;
		window->invalidate();
		return 0;
		});

	window->setOnMouseLeftDown([window](int x, int y, const Modifiers& m) -> LRESULT {
		lewy = true;
		lewy_x = x;
		lewy_y = y;
		window->invalidate();
		return 0;
		});

	window->setOnMouseLeftUp([window](int x, int y, const Modifiers& m) -> LRESULT {
		lewy = false;
		window->invalidate();
		return 0;
		});

	window->setOnMouseRightDown([window](int x, int y, const Modifiers& m) -> LRESULT {
		prawy = true;
		prawy_x = x;
		prawy_y = y;
		window->invalidate();
		return 0;
		});

	window->setOnMouseRightUp([window](int x, int y, const Modifiers& m) -> LRESULT {
		prawy = false;
		window->invalidate();
		return 0;
		});

	window->setOnBufferedDraw([window](PaintDC::Ptr& dc) {
		std::stringstream s;
		s << "x = " << mouse_x << ", y = " << mouse_y;
		if (lewy) {
			s << " Lewy (" << lewy_x << "," << lewy_y << ")";
		}
		if (prawy) {
			s << " Prawy (" << prawy_x << "," << prawy_y << ")";
		}
		std::string pozycja = s.str();

		// czyszczenie okna
		auto r = window->getClientRect();
		dc->setPenColor(Color::white);
		dc->setBrushColor(Color::white);
		dc->drawRectangle(0, 0, r.width(), r.height());

		// wypisanie zgromadzonych znaków
		dc->textOut(10, 10, pozycja);
		return 0;
		});

	window->show();
	window->update();

}