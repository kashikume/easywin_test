﻿#include <Windows.h>
#include <iostream>

#include "easywin/easywin.h"

using namespace easywin;

std::u8string tajne_haslo;

void testEditBoxes()
{
	auto window = Window::CreateTopWindow();
	window->setName(u8"Test edit boxów");

	auto edit = Edit::CreateSingleLine(window, RECT{ 10, 10, 300, 30 });
	auto multiline = Edit::CreateMultiline(window, RECT{ 10, 40, 300, 240 });
	auto password = Edit::CreatePassword(window, RECT{ 10, 250, 300, 270 });
	password->setBackground(Color::yellow);

	auto pokaz = Button::CreateButton(window, u8"Pokaż!", RECT{ 10, 280, 300, 310 });
	auto text = StaticText::Create(window, u8"Tekst statyczny", RECT{ 10,360,300,390 }, SS_CENTER);
	text->setBackground(Color::yellow);
	text->setTextColor(Color::red);

	// tekst w kontrolce ustawia się tak:
	multiline->setText(u8"Po głębinie statek płynie,\r\nTra-la-la-la chlupie fala.");

	pokaz->setOnClick([password, window]()->int {
		tajne_haslo = password->getText();
		window->invalidate();
		return 0;
		});

	// przy każdej zmianie zawartości kontrolka tekstowa wywoła tę funkcję:
	edit->setOnChange([]()->int {
		std::cout << "Changed!" << std::endl;
		return 0;
		});

	window->setOnBufferedDraw([window](PaintDC::Ptr& dc) -> LRESULT {
		dc->setPenColor(Color::white);
		dc->setBrushColor(Color::white);
		dc->drawRectangle(window->getClientRect());

		dc->textOut(100, 320, tajne_haslo);
		return 0;
		});

	window->show();
	window->update();

}