﻿#include <compare>

#include "easywin/easywin.h"

using namespace easywin;

void testBitmaps()
{
	// wczytywanie bitmapy z pliku BMP.
	auto bitmap = Bitmap::CreateFromBmpFile("res/test.bmp");
	auto scalledCopy = bitmap->getScalledCopy(0.7f);
	auto transparent_bmp = Bitmap::CreateFromBmpFile("res/miner.bmp");
	transparent_bmp->setTransparency(0xff00ff);

	auto window = Window::CreateTopWindow();
	auto icon = Icon::CreateFromTransparentBitmap(transparent_bmp);
	window->setIcon(icon);

	window->setOnBufferedDraw([window, bitmap, transparent_bmp, scalledCopy](PaintDC::Ptr& dc) {

		dc->setBrushColor(Color::gray);
		dc->setPenColor(Color::gray);
		dc->drawRectangle(window->getClientRect());

		// tak się rysuje na bitmapie
		// napiszemy na bitmapie "xxx" i narysujemy kóko
		auto bdc = bitmap->getPaintDC(dc);
		bdc->textOut(10, 10, "xxx");
		bdc->drawCircle(30, 30, 10);

		// utworzymy czyską bitmapę i na niej coś narysujemy
		auto memBuffer = Bitmap::CreateCompatibile(dc, 400, 400);
		auto mdc = memBuffer->getPaintDC(dc);
		mdc->drawRectangle(30, 30, 100, 100);
		mdc->setPixel(50, 50, Color::pink);

		// narysujemy na ekranie 2 kopie bitmapy wczytanej z pliku i zmodyfikowanej
		dc->drawBitmap(10, 10, bitmap);
		dc->drawBitmap(10, 310, bitmap);

		dc->drawBitmap(20, 90, transparent_bmp);

		// narysujemy na ekranie naszą utworzoną wcześniej bitmapę.
		dc->drawBitmap(300, 0, memBuffer);

		dc->drawBitmap(300, 300, scalledCopy);
		return 0;
		});


	window->show();
	window->update();
	window->setName(u8"Testing bitmaps");
}