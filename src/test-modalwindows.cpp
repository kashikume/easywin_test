#include <string>

#include "easywin/easywin.h"

using namespace easywin;

void testModalWindows()
{
	auto window = Window::CreateTopWindow();
	auto button = Button::CreateButton(window, u8"Open modal window", Rect{ 10,10,200,50 });
	button->setOnClick([window] {
		auto modalWindow = Window::CreateModalWindow(window, 400, 300, [](Window::Ptr win)
			{
				StaticText::Create(win, u8"This is modal window, like dialog", Rect{ 0,0,200,20 });
				win->setName(u8"Modal Window");
			});
		modalWindow->show();

		});

	window->show();
}