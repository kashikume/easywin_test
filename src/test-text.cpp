﻿#include <Windows.h>
#include <iostream>

#include "easywin/easywin.h"

using namespace easywin;

std::wstring text;

void testText()
{
	auto window = Window::CreateTopWindow();
	window->setName(u8"Test tekstu i klawiatury");

	window->setOnChar([window](WCHAR znak) -> LRESULT {
		text = text + znak;
		window->invalidate();
		return 0;
		});

	window->setOnBufferedDraw([window](PaintDC::Ptr& dc) -> LRESULT {
		dc->setBrushColor(Color::white);
		dc->setPenColor(Color::white);
		dc->drawRectangle(window->getClientRect());

		dc->textOut(10, 10, text);
		return 0;
		});

	window->show();
	window->update();

}