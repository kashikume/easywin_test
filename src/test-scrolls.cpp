﻿#include <iostream>
#include <string>
#include <sstream>

#include <Windows.h>

#include "easywin/easywin.h"

using namespace easywin;

struct _pos
{
	int x, y;
};

void testScrolls()
{
	auto window = Window::CreateTopWindow();
	window->setName(u8"Test scroli");

	auto hscroll = ScrollBar::Create(window, RECT{ 10,10,210,40 }, false);
	hscroll->setupScroll(0, 0, 1000, 100);

	auto hstatic = StaticText::Create(window, u8"", RECT{ 120,50,210,90 });
	hscroll->setOnChange([hscroll, hstatic]() {
		std::stringstream ss;

		ss << "pos: " << hscroll->getPos();
		hstatic->setText(std::u8string((char8_t*)ss.str().data()));
		return 0;
		});

	hscroll->setBackground(Color::yellow);

	auto vscroll = ScrollBar::Create(window, RECT{ 10,50,40, 300 }, true);
	vscroll->setupScroll(0, 0, 1000, 100);

	auto pos = std::make_shared<_pos>(_pos{});

	auto scrollwindow = Window::CreateClient(window, RECT{ 200,200,600,600 }, true, true);
	scrollwindow->setupScroll(true, 0, 0, 100, 10);
	scrollwindow->setupScroll(false, 0, 0, 50, 10);

	scrollwindow->setOnDraw([pos](PaintDC dc) {
		std::stringstream ss;
		ss << pos->x << "/" << pos->y << "      ";
		dc.textOut(10, 10, ss.str());
		return 0;
		});

	scrollwindow->setOnHScrollPos([scrollwindow, pos](int p) {
		pos->x = p;
		scrollwindow->invalidate();
		return 0;
		});

	scrollwindow->setOnVScrollPos([scrollwindow, pos](int p) {
		pos->y = p;
		scrollwindow->invalidate();
		return 0;
		});

	auto trackbar = Trackbar::Create(window, Rect{ 400,100,1000,130 }, false);
	trackbar->setRange(0, 100);
	trackbar->setPageSize(10);
	trackbar->setPos(30);
	trackbar->setTicFreq(10);
	trackbar->setBackground(Color::grey);
	trackbar->setOnCommand([trackbar]()
		{
			std::cout << trackbar->getPos() << std::endl;
			return 0;
		});

	window->show();
	window->update();
}